package fr.samse.bravo.ext2bravo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ext2bravoApplication {
    public static void main(String[] args) {
        SpringApplication.run(Ext2bravoApplication.class, args);
    }
}
