package fr.samse.bravo.ext2bravo.bravo.controllers;

import fr.samse.bravo.ext2bravo.ApiVersion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;


@RestController
@ApiVersion(1)
public class ProduitController {



    private static final Logger logger = LoggerFactory.getLogger(ProduitController.class);

     @GetMapping("test")
    public String testEndpoint(){
        logger.info("ici");
        return "test OK";
    }

}
